__author__ = "Daniele Boaretti (daniele.boarettig@mail.polimi.it), 2017"
from dolfin import *

import numpy as np
import pdb
import os
import sys
import warnings
from fenics_tumor import *
from mshr import *

def initialMesh():
    """
    It creates the circle mesh.

    Returns:
        The function returns the mesh for the problem.

    """

    Theta = pi/2
    a, b = 0.1, 40
    nr = 230  # divisions in r direction
    nt = 50  # divisions in theta direction
    mesh = RectangleMesh(Point(a, 0),Point( b, 1), nr, nt, 'crossed')

    # First make a denser mesh towards r=a
    x = mesh.coordinates()[:,0]
    y = mesh.coordinates()[:,1]
    s = 3

    def denser(x, y):
        return [a + (b-a)*((x-a)/(b-a))**s, y]

    x_bar, y_bar = denser(x, y)
    xy_bar_coor = np.array([x_bar, y_bar]).transpose()
    mesh.coordinates()[:] = xy_bar_coor

    def cylinder(r, s):
        return [r*np.cos(Theta*s), r*np.sin(Theta*s)]

    x_hat, y_hat = cylinder(x_bar, y_bar)
    xy_hat_coor = np.array([x_hat, y_hat]).transpose()
    mesh.coordinates()[:] = xy_hat_coor

    return mesh

def makePath():
    """
    It creates the name of the output folder and the path of the
    output file.


    Returns:
    The function returns name of the output folder and the path of the
    output file.

    """
    Comm=mpi_comm_world()

    namefolder=os.getcwd()+"/Results_stable"


    Comm.Barrier()
    if Comm.rank==0:
        if os.path.exists(namefolder):
            os.stat(namefolder)
        else:
            os.mkdir(namefolder)
    Comm.Barrier()

    filepath=namefolder+"/solutions"
    Comm.Barrier()
    if Comm.rank==0:
        if os.path.exists(filepath+".xdmf"):
            os.remove(filepath+".xdmf")
            os.remove(filepath+".h5")
    Comm.Barrier()
    return (namefolder,filepath)

if __name__ == "__main__":
    #: PETSc backend
    parameters["linear_algebra_backend"] = "PETSc"
    parameters["form_compiler"]["cpp_optimize"] = True
    parameters["form_compiler"]["representation"] = "uflacs"
    parameters["form_compiler"]["cpp_optimize_flags"] = "-O2 -ffast-math -march=native"
    parameters["allow_extrapolation"] = True
    args = [sys.argv[0]] + """
    --petsc.ksp_type bcgs
    """.split()
    parameters.parse(args)

    (namefolder,filepath)=makePath()

    # Create mesh and define function space; if the mesh is different from the
    # square (0,L) x (0,L), please set nx = 1, ny = 1, L = 1 and don't pass
    # anything to the function initialMesh
    L = 1
    nx = 1
    ny = 1
    mesh = initialMesh()

    # Initial condition.
    mn_init = Expression(('am*exp(-bm*(pow(x[0],2)+pow(x[1],2) )   )',\
            'an*exp(-bn*(pow(x[0],2)+pow(x[1],2) )   )'),am=Constant(0.1),\
                                                     bm =Constant(0.5  ),\
                                                     an =Constant(0.8),\
                                                     bn = Constant(5.0*10**-7),\
                                                     degree=2)

    # It creates the output file used to store the solution
    filehandler = XDMFFile(mpi_comm_world(),filepath+".xdmf")


    # Here I build the bc: 1 for bottom, 2 for right, 3 for up, 4 for left
    # First value in BCvec is the position, the second 1 for m, 2 for
    # n, 3 for both, the third value represent the value of the bc, which is
    # a vector in case of a bc applied on m and on n
    bcs = []

    # Set the dictionary of all physical data

    data =  {
        'mu': 1,   #viscosity of dividing cells
        'nu': 2,   #viscosity of non dividing cells
        'Pm': 30,   #Parameter for saturation of dividing cells
        'gamma': 30,     #Parameter for stiffnes of pressure law
        'L': L,     #Size of domain
        'nx': nx,   #number of subdivision on x axis in case of a square domain,
        # if the domain is not a square this value is not used in the classes
        'ny': ny,   #number of subdivision on y axis in case of a square domain,
        # if the domain is not a square this value is not used in the classes
        'dt': 0.005,   #temporal step
        'T': 0.6,    #Length od temporal domain
        'mesh': mesh,   #given mesh
        'mn_init' : mn_init,  #starting condition
        'filehandler' : filehandler #file that will contain the output
        }

    snes_solver_parameters = {"nonlinear_solver": "snes",
				  "snes_solver": {"linear_solver": "lu",
						  "maximum_iterations": 300,
						  "report": True,
						  "error_on_nonconvergence": True,
						  "line_search": 'bt',
						"method": 'default'}}

    problem = TumorClass2Dprimal(data,bcs)


    problem.solve(snes_solver_parameters)
