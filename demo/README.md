# I'm a new user, where do I start? #

If you are interested in just using tumor-dolfin for computing biology
simulations, start by looking at 2D demos.

If you are interested in just using tumor-dolfin for understanding a possible
simulation of the dynamic of a tumor cell, start by looking at 1D demos.
