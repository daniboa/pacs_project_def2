__author__ = "Daniele Boaretti (daniele.boarettig@mail.polimi.it), 2017"
from dolfin import *

import numpy as np
import pdb
import os
import sys
import warnings
from fenics_tumor import *

def initialMesh(Lx=-6,Rx=10,nx=100):
    """Summary line.

    It creates the mesh, with default length L =30, nx=30 subdivisions on
    x axis, ny=30 subdivision on y axis

    Args:
        Lx (real): Left extremum of the 1D domain
        Rx (real): Right extremum of the 1D domain
        nx (int): Number of subdivision on x axis


    Returns:
        The function returns the mesh for the problem.

    """


    mesh = IntervalMesh(nx, Lx, Rx)

    return mesh

def makePath():
    """

    It creates the name of the output folder and the path of the
    output file.


    Returns:
    The function returns name of the output folder and the path of the
    output file.

    """
    Comm=mpi_comm_world()

    namefolder=os.getcwd()+"/Results"


    Comm.Barrier()
    if Comm.rank==0:
        if os.path.exists(namefolder):
            os.stat(namefolder)
        else:
            os.mkdir(namefolder)
    Comm.Barrier()

    filepath=namefolder+"/solutions"
    Comm.Barrier()
    if Comm.rank==0:
        if os.path.exists(filepath+".xdmf"):
            os.remove(filepath+".xdmf")
            os.remove(filepath+".h5")
    Comm.Barrier()
    return (namefolder,filepath)

if __name__ == "__main__":
    #: PETSc backend
    parameters["linear_algebra_backend"] = "PETSc"
    parameters["form_compiler"]["cpp_optimize"] = True
    parameters["form_compiler"]["representation"] = "uflacs"
    parameters["form_compiler"]["cpp_optimize_flags"] = "-O2 -ffast-math -march=native"
    parameters["allow_extrapolation"] = True
    args = [sys.argv[0]] + """
    --petsc.ksp_type bcgs
    """.split()
    parameters.parse(args)

    (namefolder,filepath)=makePath()
    # Create mesh and define function space

    Lx = -6.
    Rx = 10.
    nx = 100

    mesh=initialMesh(Lx,Rx,nx)

    #: It creates the output file used to store the solution
    filehandler=XDMFFile(mpi_comm_world(),filepath+".xdmf")






    data =  {
        'mu': 1,   #viscosity of dividing cells
        'nu': 2,   #viscosity of non dividing cells
        'Pm': 30,   #Parameter for saturation of dividing cells, put 0 if no
                    #reaction
        'gamma': 4,     #Parameter for stiffnes of pressure law
        'Lx': Lx,     #Left estremum of domain
        'Rx': Rx,   #Right estremum of domain
        'nx': nx,   #number of subdivision on x axis
        'dt': 0,   #temporal step, put 0 for stationary case
        'T': 300*0.005,     #Length od temporal domain
        'mesh': mesh,   #given mesh
        'mn_init' : 0.,  #starting condition: I have to put something, for
            #example 0, then I redefine it
        'system': 'm',  #put m if you want the system with only m, otherwise m_n
        'filehandler' : filehandler #file that will contain the output
        }

    # Initial condition.
    data['mn_init'] = Expression(('pow( (Pm/(1./gam +1.)) ,(1./gam)) '), Pm=data['Pm'],\
            gam=data['gamma'],degree=1)


    # Define the solver parameters
    snes_solver_parameters = {"nonlinear_solver": "snes",
                              "snes_solver": {"linear_solver": "lu",
                                              "maximum_iterations": 40,
                                              "report": True,
                                              "error_on_nonconvergence": True,
                                              "line_search": 'bt',
                                              "method": 'default'}}

    #: Here I build the bc: 1
    #: First value in BCvec is the position, 1 for left, 2 for right,
    #  the second value represent the value of the bc, which is
    #: a vector in case of a bc applied on m and on n
    bcs = [[1, Constant(((data['Pm']/(1.+1./data['gamma']))**(1./data['gamma'])))]\
            ,[2, 0. ]]

    #Set the dictionary of all physical data


    problem = TumorClass1Dprimal(data,bcs)


    problem.solve(snes_solver_parameters)
