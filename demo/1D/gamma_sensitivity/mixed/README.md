# README #

### What are these files? ###
This folder contains the demo files for the sensitivity on gamma with Mixed
method for the full 1D system.


### How to run the code? ###
To tun the file, simply from the command line

python demo_g2_gamma8.py

for the case gamma=8, otherwise

python demo_g2_gamma100.py

for the case gamma=100
