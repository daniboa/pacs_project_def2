"""
Unit tests for SolverClass
"""
__all__ = ['SolverClass']
from dolfin import *

class SolverClass (NonlinearVariationalSolver):
    def __init__(self,par, Tumorproblem):
        NonlinearVariationalSolver.__init__(self,Tumorproblem)
        #We first receive the variational problem and then we setup the parameters
        self.SnesDict = {}
        #self.problem = VariationalProblem
        SolverClass.set_parameters(par)
        self.setup_tumor_solver()

    @staticmethod
    def set_parameters( par):
        """
        Here I define the dictionary of the SolverClass through the user
            defined dictionary which contains the parameters for the snes
                solver.

        Args:
            par (dict): the user defined dictionary for the parameters of the
                snes solver


        """
    #we setup the parameters for the snes solver
        SolverClass.SnesDict = {"nonlinear_solver": par['nonlinear_solver'],
                                  "snes_solver":
                             {"linear_solver":
                                par['snes_solver']['linear_solver'],
                             "maximum_iterations":
                                par['snes_solver']['maximum_iterations'],
                             "report":
                                par['snes_solver']['report'],
                             "error_on_nonconvergence":
                                par['snes_solver']['error_on_nonconvergence'],
                             "line_search":
                                par['snes_solver']['line_search'],
                             "method":
                                par['snes_solver']['method']}}


    def setup_tumor_solver(self):
        """
        Here I update the solver parameters, thanks to the inheritance of the
            SolverClass.


        """
        self.parameters.update(SolverClass.SnesDict)
