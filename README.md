# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is for the development of the PACS Project of Daniele Boaretti,
	with Davide Baroli and Tommaso Lorenzi as tutors.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* You have to use the FeniCS environment (and python)
* Dependencies
* Database configuration
* See the README file in the folder of the code where you want to run the code 
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* The owner of the Repo is Daniele Boaretti (daniele.boaretti@mail.polimi.it)
* Team contacts: Davide Baroli davide.baroli1@gmail.com , Tommaso Lorenzi tl47@st-andrews.ac.uk